# plataforma-investimento-angularjs

Projeto implementado para exercício de angularjs com testes unitários.

# Passos para rodar o projeto: 
 - fazer "git clone"
 - rodar "npm install"
 
# Para rodar os testes unitários:
 - "karma start"
 
# Para rodar o projeto:
 - "npm start"
 - Acessar "http://localhost:3000/"
 